###### La aplicación tiene unos estilos de botones y EditText personalizado.
###### También tiene un icono personalizado
![](http://yeray697.esy.es/images/0.png)
# 1. Modificar la aplicación Conversor de moneda para añadir un cuadro de texto en el que se guarde el cambio entre euros y dólares. Cuando se realice la conversión, se usará el valor existente en ese cuadro de texto.
###### Al seleccionar una opción de los radiobuttons, bloquea el EditText respectivo.
###### El conversor tiene unos valores de conversión por defecto, los cuáles se pueden cambiar.
###### En caso de haber un error con los valores añadidos, coge los que tiene por defecto.
![](http://yeray697.esy.es/images/1.png)

# 2. Crear una aplicación que pida un valor en centímetros y lo convierta a pulgadas.
###### Es un conversor algo más simple, sólo tiene una dirección, de centímetros a pulgadas.
![](http://yeray697.esy.es/images/2.png)
###### Tiene un bug, el cual no sé por qué ocurre, que es que el estilo personalizado no se aplica.
###### En la imagen se ve a la izquierda cómo se ve en Android Studio, y a la derecha cómo se ve ejecutado.
![](http://yeray697.esy.es/images/bug.png)
# 3. Crear una aplicación que pida una dirección web y muestre un botón. Cuando se pulse el botón, aparecerá una nueva ventana (una nueva actividad) en la que se mostrará la web pedida.
###### La aplicación intenta cargar la web elegida. En caso de error, vuelve al Activity para introducir otra web, y muestra un Toast con el error
###### La aplicación controla si tiene www. y http://
![](http://yeray697.esy.es/images/3.1.png)
![](http://yeray697.esy.es/images/3.2.png)
# 4. Cambiar la aplicación Contador de cafés para que reproduzca un sonido cada vez que el contador de tiempo llega a 0, además de incrementar el contador de cafés
###### La aplicación tiene un Switch, para indicar si la cuenta es ascendente o descendente.
###### Cuando el número de cafés llega a 10, se desactivan los botones, menos el de reiniciar cafés.
###### Cuando el temporizador comienza, se bloquean todos los botones menos el switch, y el botón comenzar se convierte en Cancelar.
![](http://yeray697.esy.es/images/4.1.png)
![](http://yeray697.esy.es/images/4.2.png)
![](http://yeray697.esy.es/images/4.3.png)

# 5. Crear una aplicación propia similar a las de los ejercicios anteriores.
###### La aplicación elegida ha sido un tres en raya. Tiene dos modos, el de un jugador, y el de dos jugadores.
###### El de un jugador presenta varios niveles de dificultad.
##### Menú
![](http://yeray697.esy.es/images/5.1.png)
##### Menú de Un jugador
![](http://yeray697.esy.es/images/5.2.png)
##### Ejemplo de una partida
![](http://yeray697.esy.es/images/5.3.png)
