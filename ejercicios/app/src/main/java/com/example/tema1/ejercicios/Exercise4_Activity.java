package com.example.tema1.ejercicios;

import android.media.MediaPlayer;
import android.os.CountDownTimer;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

public class Exercise4_Activity extends AppCompatActivity implements View.OnClickListener {

    private static final long ONESECOND = 999;
    private static final long TIMEDEFAULT = 300050; //5 minutes
    private static final long INTERVAL = 5000; //5 seconds
    private Button btLessTime, btMoreTime, btRestart;
    private TextView tvTime, tvNumberOfCoffees;
    private Button btStart;
    private Switch swOrder;
    private static MyCountDownTimer timer;
    private int time;   //Time selected (it is not the same time while running)
    private boolean timeOn; //If timeOn is true, buttons are disabled
    private boolean order; //If order is true, timer will be ascendant

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise4);
        //Changing the title
        setTitle(R.string.title4);
        //Setting controls
        btLessTime = (Button) findViewById(R.id.btLessTime4);
        btMoreTime = (Button) findViewById(R.id.btMoreTime4);
        btRestart = (Button) findViewById(R.id.btRestart4);
        tvTime = (TextView) findViewById(R.id.tvTime4);
        tvNumberOfCoffees = (TextView) findViewById(R.id.tvNumberOfCoffees4);
        btStart = (Button) findViewById(R.id.btStart4);
        swOrder = (Switch) findViewById(R.id.swOrder);
        //Click events
        btLessTime.setOnClickListener(this);
        btMoreTime.setOnClickListener(this);
        btStart.setOnClickListener(this);
        btRestart.setOnClickListener(this);
        swOrder.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                order = b;
            }
        });
        //Time
        timeOn = false;
        time = (int)TIMEDEFAULT;
        order = false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btStart4:
                if (timeOn)
                    cancel();
                else
                    start();
                break;
            case R.id.btLessTime4:
                lessTime();
                break;
            case R.id.btMoreTime4:
                moreTime();
                break;

            case R.id.btRestart4:
                restart();
                break;
            default:
                break;
        }
    }

    /**
     * Restart the number of coffees
     */
    private void restart() {
        tvNumberOfCoffees.setText(getResources().getString(R.string.tvNumberOfCoffee4));
        btStart.setEnabled(true);
        btLessTime.setEnabled(true);
        btMoreTime.setEnabled(true);
    }

    /**
     * Cancel the chrono
     */
    private void cancel() {
        timer.cancel();
        tvTime.setText(msToTime(time));
        btStart.setText(getResources().getString(R.string.btStart4));
        timeOn = false;
        btRestart.setEnabled(true);
        btLessTime.setEnabled(true);
        btMoreTime.setEnabled(true);
    }

    /**
     * Start the chrono
     */
    private void start() {
        timer = new MyCountDownTimer(time,ONESECOND);
        timer.start();
        btStart.setText(getResources().getString(R.string.btCancel4));
        timeOn = true;
        btRestart.setEnabled(false);
        btLessTime.setEnabled(false);
        btMoreTime.setEnabled(false);
    }

    /**
     * Subtract 5 seconds to the actual time
     */
    private void lessTime() {
        if (time > (INTERVAL * 2)) { //Because if time is 0, it would be useless
            time -= INTERVAL;
        }
        tvTime.setText(msToTime(time));
    }

    /**
     * Add 5 seconds to the actual time
     */
    private void moreTime() {
        time += INTERVAL;
        tvTime.setText(msToTime(time));
    }

    /**
     * Set time on tvTime
     * @param time Time to set
     */
    protected void changeTime(String time){
        tvTime.setText(time);
    }

    /**
     * Method called when timer finish
     */
    protected void timerFinished(){
        //Restarting controls
        btLessTime.setEnabled(true);
        btMoreTime.setEnabled(true);
        btRestart.setEnabled(true);
        btStart.setEnabled(true);
        tvTime.setText(msToTime(time));
        btStart.setText(getResources().getString(R.string.btStart4));
        timeOn = false;
        //Adding one coffee
        String text = String.valueOf(Integer.parseInt(tvNumberOfCoffees.getText().toString()) + 1);
        tvNumberOfCoffees.setText(text);
        //Sound the alarm
        MediaPlayer mp = MediaPlayer.create(this, R.raw.alarm);
        mp.start();
        //Showing an alert dialog if number of coffees is equal to 10
        if(Integer.parseInt(tvNumberOfCoffees.getText().toString()) == 10) {
            btLessTime.setEnabled(false);
            btMoreTime.setEnabled(false);
            btStart.setEnabled(false);
            AlertDialog.Builder popup = new AlertDialog.Builder(this);
            popup.setTitle("FIN");
            popup.setMessage("Has llegado al máximo número de cafés");
            popup.setPositiveButton("Ok", null);
            popup.show();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (timer != null)
            timer.cancel();
    }

    /**
     * Convert milliseconds to a string with the format xx:xx
     * @param time Time to convert
     * @return Time converted
     */
    private String msToTime(int time) {
        int minutes, seconds;
        String minutesAux, secondsAux;
        //Getting minutes
        minutes = (time / 1000)/60;
        if (minutes == 0)
            minutesAux = "00";
        else if (minutes < 10)
            minutesAux = "0"+minutes;
        else
            minutesAux = String.valueOf(minutes);
        //Getting seconds
        seconds = (time / 1000)%60;
        if (seconds == 0)
            secondsAux= "00";
        else if (seconds < 10)
            secondsAux = "0"+seconds;
        else
            secondsAux = String.valueOf(seconds);
        return minutesAux+":"+secondsAux;
    }

    /**
     * My chrono
     */
    protected class MyCountDownTimer extends CountDownTimer {

    /**
     * @param millisInFuture    The number of millis in the future from the call
     *                          to {@link #start()} until the countdown is done and {@link #onFinish()}
     *                          is called.
     * @param countDownInterval The interval along the way to receive
     *                          {@link #onTick(long)} callbacks.
     */
    public MyCountDownTimer(long millisInFuture, long countDownInterval) {
        super(millisInFuture, countDownInterval);
    }

    @Override
    public void onTick(long millisUntilFinished) {
        String timeConverted;
        if(!order){ //Falling
            timeConverted = msToTime((int) millisUntilFinished);
        }
        else{ //Ascendant
            int timeAux = (int) (time - millisUntilFinished);
            timeConverted = msToTime(timeAux);
        }
        changeTime(timeConverted);
    }

    @Override
    public void onFinish() {
        timerFinished();
    }
}

}
