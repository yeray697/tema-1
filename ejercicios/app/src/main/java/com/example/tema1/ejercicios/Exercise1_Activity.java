package com.example.tema1.ejercicios;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import java.util.Locale;

public class Exercise1_Activity extends AppCompatActivity implements View.OnClickListener {

    //
    // 1. Modificar la aplicación Conversor de moneda para añadir un cuadro de texto en el que
    // se guarde el cambio entre euros y dólares. Cuando se realice la conversión, se usará el
    // valor existente en ese cuadro de texto.
    //

    private static final String NONUMBERERROR = "Introduce un número";

    private EditText etDollar, etEuro;
    private RadioButton rbDtoE, rbEtoD;
    private Button btConvert;
    private EditText etCurrencyEuro, etCurrencyDollar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise1);
        //Changing the title
        setTitle(R.string.title1);
        //Setting controls
        etDollar = (EditText) findViewById(R.id.etDollar1);
        etEuro = (EditText) findViewById(R.id.etEuro1);
        rbDtoE = (RadioButton) findViewById(R.id.rbDtoE1);
        rbEtoD = (RadioButton) findViewById(R.id.rbEtoD1);
        btConvert = (Button) findViewById(R.id.btConvert1);
        etCurrencyEuro = (EditText) findViewById(R.id.etCurrencyEuro);
        etCurrencyDollar = (EditText) findViewById(R.id.etCurrencyDollar);
        //Click events
        rbDtoE.setOnClickListener(this);
        rbEtoD.setOnClickListener(this);
        btConvert.setOnClickListener(this);
        //Focusing etDollar EditText
        etDollar.requestFocus();
        //Disabling etEuro EditText
        etEuro.setEnabled(false);
        //Setting defaults currency conversion values
        etCurrencyDollar.setText(String.format(Locale.US, "%.4f", Utilities.ONEDOLLARISXEUROS));
        etCurrencyEuro.setText(String.format(Locale.US, "%.4f", Utilities.ONEEUROISXDOLLAR));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btConvert1:
                Convert();
                break;
            case R.id.rbDtoE1:
                radioButtonFocus(true);
                break;
            case R.id.rbEtoD1:
                radioButtonFocus(false);
                break;
            default:
                break;
        }
    }

    /**
     * Method that call Utilities static class, anc call his respective methods to convert according to which radiobutton is checked
     */
    private void Convert() {
        try {
            double currency;
            if (rbDtoE.isChecked()) //If it is checked, we convert dollars to euros
            {
                //Getting dollars
                currency = Double.parseDouble(etDollar.getText().toString());
                //Converting it to euros
                double result;
                if(Double.parseDouble(etCurrencyDollar.getText().toString()) == Utilities.ONEDOLLARISXEUROS ||
                        Double.parseDouble(etCurrencyDollar.getText().toString()) <= 0)
                    result = Utilities.dollarToEuro(currency);
                else
                    result = Utilities.dollarToEuro(currency, Double.parseDouble(etCurrencyDollar.getText().toString()));
                //Showing in the other EditText, rounded to 4 decimals
                etEuro.setText(String.format(Locale.US, "%.4f", result));
            } else //else, we convert euros to dollars
            {
                //Getting euros
                currency = Double.parseDouble(etEuro.getText().toString());
                //Converting it to dollars
                double result;
                if(Double.parseDouble(etCurrencyEuro.getText().toString()) == Utilities.ONEEUROISXDOLLAR ||
                        Double.parseDouble(etCurrencyEuro.getText().toString()) <= 0)
                    result = Utilities.euroToDollar(currency);
                else
                    result = Utilities.euroToDollar(currency, Double.parseDouble(etCurrencyEuro.getText().toString()));
                //Showing in the other EditText, rounded to 4 decimalss
                etDollar.setText(String.format(Locale.US, "%.4f", result));
            }
        }
        catch (Exception ex) {
            //If there is an error, it is because there is nothing to convert
            Toast.makeText(this, NONUMBERERROR, Toast.LENGTH_SHORT).show();
        }
    }


    /**
     * Used to enable the respective editText when his radiobutton is checked
     * @param checked Used to know what radiobutton is checked: true = rbDtoE1.isChecked(); false = rbEtoD1.isChecked()
     */
    private void radioButtonFocus(boolean checked) {
        etDollar.setEnabled(checked);
        etEuro.setEnabled(!checked);
        if (checked)
            etDollar.requestFocus();
        else
            etEuro.requestFocus();
    }
}
