package com.example.tema1.ejercicios;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class TicTacToe_Activity extends AppCompatActivity implements View.OnClickListener {

    protected static final String MODEINTENT = "mode";
    protected static final String MODE1INTENT = "one";
    protected static final String MODE2INTENT = "two";
    protected static final String LEVELINTENT = "level";
    protected static final int LEVEL1INTENT = 1;
    protected static final int LEVEL2INTENT = 2;
    protected static final int LEVEL3INTENT = 3;

    private static TicTacToe game;
    private Button btBox1, btBox2, btBox3, btBox4, btBox5, btBox6, btBox7, btBox8, btBox9;
    private TextView tvTurn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tic_tac_toe);
        //Setting the title
        setTitle(R.string.title5);
        //Setting controls
        btBox1 = (Button) findViewById(R.id.btBox1);
        btBox2 = (Button) findViewById(R.id.btBox2);
        btBox3 = (Button) findViewById(R.id.btBox3);
        btBox4 = (Button) findViewById(R.id.btBox4);
        btBox5 = (Button) findViewById(R.id.btBox5);
        btBox6 = (Button) findViewById(R.id.btBox6);
        btBox7 = (Button) findViewById(R.id.btBox7);
        btBox8 = (Button) findViewById(R.id.btBox8);
        btBox9 = (Button) findViewById(R.id.btBox9);
        tvTurn = (TextView) findViewById(R.id.tvTurnPlayer);
        //Click events
        btBox1.setOnClickListener(this);
        btBox2.setOnClickListener(this);
        btBox3.setOnClickListener(this);
        btBox4.setOnClickListener(this);
        btBox5.setOnClickListener(this);
        btBox6.setOnClickListener(this);
        btBox7.setOnClickListener(this);
        btBox8.setOnClickListener(this);
        btBox9.setOnClickListener(this);
        //Getting data from previous intent
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        //Getting gamemode: 1 or 2 players
        String mode = bundle.getString(MODEINTENT);
        int level = -1;         //Inicialized as -1. That means it is one player's mode so it doesn't have difficulty
        //If it is one player's mode, we get difficulty level
        if (mode.equals(MODE1INTENT)) {
            level = bundle.getInt(LEVELINTENT);
        }
        //Creating game
        if (mode.equals(MODE1INTENT)) {
            game = new TicTacToe(level);
        }
        else
            game = new TicTacToe();
        setTurn();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btBox1:
                setPiece(v, 0, 0);
                break;
            case R.id.btBox2:
                setPiece(v, 0, 1);
                break;
            case R.id.btBox3:
                setPiece(v, 0, 2);
                break;
            case R.id.btBox4:
                setPiece(v, 1, 0);
                break;
            case R.id.btBox5:
                setPiece(v, 1, 1);
                break;
            case R.id.btBox6:
                setPiece(v, 1, 2);
                break;
            case R.id.btBox7:
                setPiece(v, 2, 0);
                break;
            case R.id.btBox8:
                setPiece(v, 2, 1);
                break;
            case R.id.btBox9:
                setPiece(v, 2, 2);
                break;
        }
    }

    /**
     * Player set a piece on the board
     * @param v Box button pressed where piece image will be set
     * @param x X coordinate
     * @param y Y coordinate
     */
    private void setPiece(View v, int x, int y){
        //First one, we try to put a piece.
        //If it is not possible, it shows it with Toast
        if (game.setPiece(x,y)) {
            Button btAux = (Button) v;
            //Setting piece image on the board
            setImageButton(btAux);
            //Checking if game ended when player set a piece
            if (!checkIfThereIsAWinner())
                nextTurn();
            //If game is versus AI, and game has not ended, AI set a piece
            if (game.isGameOn()){
                if (game.getGameMode() == 1){
                        setPieceLevel();
                    //Checking if game ended when player set a piece
                    checkIfThereIsAWinner();
                }
            }
        }
        else
            Toast.makeText(this, R.string.pieceAlreadyOccupied, Toast.LENGTH_SHORT).show();
    }

    /**
     * Check if game is on, and who is the winner
     * @return Return if there is a winner
     */
    private boolean checkIfThereIsAWinner() {
        boolean result = false;
        //If game has ended, get who is the winner
        if (!game.isGameOn()){
            String titulo = "";
            switch (game.getWinner()){
                case 0:
                    titulo = getResources().getString(R.string.dialogTitleTie);
                    break;
                case 1:
                    titulo = getResources().getString(R.string.dialogTitleWin1);
                    break;
                case 2:
                    titulo = getResources().getString(R.string.dialogTitleWin2);
                    break;
            }
            //Show an alertDialog with winner's name
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(titulo);
            builder.setMessage(R.string.dialogMessage);
            builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            recreate();
                        }
                    });
            builder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
            builder.setCancelable(false);
            builder.show();
            result = true;
        }
        return result;
    }

    /**
     * AI set a piece depending of level set
     */
    private void setPieceLevel() {
        int[] positions = null;
        int x,y;
        Button btAux = null;
        if (game.getDifficulty() == 1)
            positions = game.setPieceEasy();
        else if (game.getDifficulty() == 2)
            positions = game.setPieceMedium();
        else if (game.getDifficulty() == 3)
            positions = game.setPieceHard();
        //Saving positions
        x = positions[0];
        y = positions[1];
        game.setPiece(x,y);
        //Parse positions to know what buttons was "pressed" by AI
        if (x == 0 && y == 0)
            btAux = btBox1;
        else if (x == 0 && y == 1)
            btAux = btBox2;
        else if (x == 0 && y == 2)
            btAux = btBox3;
        else if (x == 1 && y == 0)
            btAux = btBox4;
        else if (x == 1 && y == 1)
            btAux = btBox5;
        else if (x == 1 && y == 2)
            btAux = btBox6;
        else if (x == 2 && y == 0)
            btAux = btBox7;
        else if (x == 2 && y == 1)
            btAux = btBox8;
        else if (x == 2 && y == 2)
            btAux = btBox9;
        setImageButton(btAux);
        nextTurn();
    }

    /**
     * Changing turn
     */
    private void nextTurn() {
        game.nextPlayer();
        if (game.getTurn())
            tvTurn.setText(R.string.turnA5);
        else
            tvTurn.setText(R.string.turnB5);
    }

    /**
     * Set the button pressed with player's piece image
     * @param btAux Button that will be set with the image
     */
    private void setImageButton(Button btAux) {
        if (game.getTurn()) {
            btAux.setBackgroundResource(R.drawable.piece_b);
        } else {
            btAux.setBackgroundResource(R.drawable.piece_a);
        }
        //Resizing image, because on API 24 set buttons with random position (?)
        Drawable aux = btAux.getBackground();
        btAux.setBackground(resize(aux, btAux));
    }

    /**
     * Resize an image with the same size as the Button passed on
     * @param image Image that is going to be resized
     * @param btAux Button where is going to be the image displayed
     * @return Return the image with the new size
     */
    private Drawable resize(Drawable image, Button btAux) {
        int width = btAux.getWidth(),
                height = btAux.getHeight();
        Bitmap b = ((BitmapDrawable)image).getBitmap();
        Bitmap bitmapResized = Bitmap.createScaledBitmap(b, width, height, false);
        return new BitmapDrawable(getResources(), bitmapResized);
    }

    /**
     * Set on tvTurn TextView whose turn
     */
    private void setTurn() {
        if(game != null) {
            if (game.getTurn())
                tvTurn.setText(getResources().getText(R.string.turnA5));
            else
                tvTurn.setText(getResources().getText(R.string.turnB5));
        }
    }
}