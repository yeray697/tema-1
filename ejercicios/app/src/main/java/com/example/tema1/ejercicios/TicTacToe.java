package com.example.tema1.ejercicios;

import java.util.Random;

/**
 * Created by usuario on 27/09/16.
 */

public class TicTacToe {

    private final char EMPTY = '*';
    private char[][] board = {
        {EMPTY,EMPTY,EMPTY},
        {EMPTY,EMPTY,EMPTY},
        {EMPTY,EMPTY,EMPTY}
    };

    private final char PIECEA = 'o';
    private final char PIECEB = 'x';

    private boolean turn;   //Show whose turn
                            //true = player 1
                            //false = player 2

    private boolean gameOn; //It means if there is an active game

    private int winner;     //-1 = Active game
                            //0  = Tie
                            //1  = Player 1 wins
                            //2  = Player 2 wins

    private int difficulty; //-1 = Two players mode selected
                            //1 = Easy
                            //2 = Medium
                            //3 = Hard

    private int gameMode;   //1 = 1 player
                            //2 = 2 players

    private boolean boardFilled; //True = Players can not put more piece

    //Booleans to check if player 1 has won
    private boolean row11, row21 , row31;
    private boolean column11 , column21, column31;
    private boolean diagonal11, diagonal21;

    //Booleans to check if player 2 has won
    private boolean row12, row22, row32;
    private boolean column12, column22, column32;
    private boolean diagonal12, diagonal22;

    //Booleans to check if AI can cut you three in a row
    public boolean field11, field21, field31, field41, field51, field61, field71, field81, field91;

    //Booleans to check if AI can make three in a row
    public boolean field12, field22, field32, field42, field52, field62, field72, field82, field92;

    /**
     * One player constructor
     */
    public TicTacToe(int difficulty) {
        this.gameMode = 1;
        this.difficulty = difficulty;
        this.winner = -1;
        this.gameOn = true;
        this.turn = true;
    }

    /**
     * Two players constructor
     */
    public TicTacToe (){
        this.difficulty = -1;
        this.winner = -1;
        this.gameOn = true;
        this.turn = true;
        this.gameMode = 2;
    }

    /**
     * Set a piece on the place passed on
     * @param x X coordinate
     * @param y Y coordinate
     * @return Return true if piece has been put correctly
     */
    public boolean setPiece(int x, int y){
        boolean correct = true;
        char piece;
        if (!this.turn)
            piece = this.PIECEA;
        else
            piece = this.PIECEB;
        try {
            if (this.board[x][y] == this.EMPTY) {
                this.board[x][y] = piece;
            }
            else
                correct = false;
        } catch (Exception ex) {
            correct = false;
        }

        //If piece has been put correctly, we check if game has end
        if (correct) {
            checkGame();
            if (gameMode == 1) { //If it is a 1 player game, we update booleans so AI can make his choice
                //AI can stop you 3 in a row:
                field11 = ((board[0][1] == PIECEB && board[0][2] == PIECEB) || (board[1][0] == PIECEB && board[2][0] == PIECEB) || (board[1][1] == PIECEB && board[2][2] == PIECEB)) && board[0][0] == EMPTY; //(0,0)
                field21 = ((board[0][0] == PIECEB && board[0][2] == PIECEB) || (board[1][1] == PIECEB && board[2][1] == PIECEB)) && board[0][1] == EMPTY; //(0,1)
                field31 = ((board[0][0] == PIECEB && board[0][1] == PIECEB) || (board[1][2] == PIECEB && board[2][2] == PIECEB) || (board[1][1] == PIECEB && board[2][0] == PIECEB)) && board[0][2] == EMPTY; //(0,2)
                field41 = ((board[1][1] == PIECEB && board[1][2] == PIECEB) || (board[0][0] == PIECEB && board[2][0] == PIECEB)) && board[1][0] == EMPTY; //(1,0)
                field51 = ((board[1][0] == PIECEB && board[1][2] == PIECEB) || (board[0][1] == PIECEB && board[2][1] == PIECEB) ||
                        (board[0][0] == PIECEB && board[2][2] == PIECEB) || (board[0][2] == PIECEB && board[2][0] == PIECEB)) && board[1][1] == EMPTY; //(1,1)
                field61 = ((board[1][0] == PIECEB && board[1][1] == PIECEB) || (board[0][2] == PIECEB && board[2][2] == PIECEB)) && board[1][2] == EMPTY; //(1,2)
                field71 = ((board[0][0] == PIECEB && board[1][0] == PIECEB) || (board[2][1] == PIECEB && board[2][2] == PIECEB) || (board[1][1] == PIECEB && board[0][2] == PIECEB)) && board[2][0] == EMPTY; //(2,0)
                field81 = ((board[0][1] == PIECEB && board[1][1] == PIECEB) || (board[2][0] == PIECEB && board[2][2] == PIECEB)) && board[2][1] == EMPTY; //(2,1)
                field91 = ((board[0][2] == PIECEB && board[1][2] == PIECEB) || (board[2][0] == PIECEB && board[2][1] == PIECEB) || (board[1][1] == PIECEB && board[0][0] == PIECEB)) && board[2][2] == EMPTY; //(2,2)
                //AI can make 3 in a row
                field12 = ((board[0][1] == PIECEA && board[0][2] == PIECEA) || (board[1][0] == PIECEA && board[2][0] == PIECEA) || (board[1][1] == PIECEA && board[2][2] == PIECEA)) && board[0][0] == EMPTY; //(0,0)
                field22 = ((board[0][0] == PIECEA && board[0][2] == PIECEA) || (board[1][1] == PIECEA && board[2][1] == PIECEA)) && board[0][1] == EMPTY; //(0,1)
                field32 = ((board[0][0] == PIECEA && board[0][1] == PIECEA) || (board[1][2] == PIECEA && board[2][2] == PIECEA) || (board[1][1] == PIECEA && board[2][0] == PIECEA)) && board[0][2] == EMPTY; //(0,2)
                field42 = ((board[1][1] == PIECEA && board[1][2] == PIECEA) || (board[0][0] == PIECEA && board[2][0] == PIECEA)) && board[1][0] == EMPTY; //(1,0)
                field52 = ((board[1][0] == PIECEA && board[1][2] == PIECEA) || (board[0][1] == PIECEA && board[2][1] == PIECEA) ||
                        (board[0][0] == PIECEA && board[2][2] == PIECEA) || (board[0][2] == PIECEA && board[2][0] == PIECEA)) && board[1][1] == EMPTY; //(1,1)
                field62 = ((board[1][0] == PIECEA && board[1][1] == PIECEA) || (board[0][2] == PIECEA && board[2][2] == PIECEA)) && board[1][2] == EMPTY; //(1,2)
                field72 = ((board[0][0] == PIECEA && board[1][0] == PIECEA) || (board[2][1] == PIECEA && board[2][2] == PIECEA) || (board[1][1] == PIECEA && board[0][2] == PIECEA)) && board[2][0] == EMPTY; //(2,0)
                field82 = ((board[0][1] == PIECEA && board[1][1] == PIECEA) || (board[2][0] == PIECEA && board[2][2] == PIECEA)) && board[2][1] == EMPTY; //(2,1)
                field92 = ((board[0][2] == PIECEA && board[1][2] == PIECEA) || (board[2][0] == PIECEA && board[2][1] == PIECEA) || (board[1][1] == PIECEA && board[0][0] == PIECEA)) && board[2][2] == EMPTY; //(2,2)
            }
        }
        return correct;
    }

    /**
     * Check if game ended, and who is the winner
     */
    private void checkGame(){
        //Board is filled:
        boardFilled  =
                (this.board[0][0] != EMPTY) && (this.board[0][1] != EMPTY) && (this.board[0][2] != EMPTY) &&
                        (this.board[1][0] != EMPTY) && (this.board[1][1] != EMPTY) && (this.board[1][2] != EMPTY) &&
                        (this.board[2][0] != EMPTY) && (this.board[2][1] != EMPTY) && (this.board[2][2] != EMPTY);

        //Player 1 won:
        row11 = (board[0][0] == PIECEA) && (board[0][1] == PIECEA) && (board[0][2] == PIECEA);
        row21 = (board[1][0] == PIECEA) && (board[1][1] == PIECEA) && (board[1][2] == PIECEA);
        row31 = (board[2][0] == PIECEA) && (board[2][1] == PIECEA) && (board[2][2] == PIECEA);
        column11 = (board[0][0] == PIECEA) && (board[1][0] == PIECEA) && (board[2][0] == PIECEA);
        column21 = (board[0][1] == PIECEA) && (board[1][1] == PIECEA) && (board[2][1] == PIECEA);
        column31 = (board[0][2] == PIECEA) && (board[1][2] == PIECEA) && (board[2][2] == PIECEA);
        diagonal11 = (board[0][0] == PIECEA) && (board[1][1] == PIECEA) && (board[2][2] == PIECEA);
        diagonal21 = (board[0][2] == PIECEA) && (board[1][1] == PIECEA) && (board[2][0] == PIECEA);

        //Player 2 won:
        row12 = (board[0][0] == PIECEB) && (board[0][1] == PIECEB) && (board[0][2] == PIECEB);
        row22 = (board[1][0] == PIECEB) && (board[1][1] == PIECEB) && (board[1][2] == PIECEB);
        row32 = (board[2][0] == PIECEB) && (board[2][1] == PIECEB) && (board[2][2] == PIECEB);
        column12 = (board[0][0] == PIECEB) && (board[1][0] == PIECEB) && (board[2][0] == PIECEB);
        column22 = (board[0][1] == PIECEB) && (board[1][1] == PIECEB) && (board[2][1] == PIECEB);
        column32 = (board[0][2] == PIECEB) && (board[1][2] == PIECEB) && (board[2][2] == PIECEB);
        diagonal12 = (board[0][0] == PIECEB) && (board[1][1] == PIECEB) && (board[2][2] == PIECEB);
        diagonal22 = (board[0][2] == PIECEB) && (board[1][1] == PIECEB) && (board[2][0] == PIECEB);

        if (this.turn){
            //Player 1 won:
            if (row12 || row22 || row32 || column12 || column22 || column32 || diagonal12 || diagonal22) {
                winner = 1;
                gameOn = false;
            }
        }
        else {
            //Player 2 won:
            if (row11 || row21 || row31 || column11 || column21 || column31 || diagonal11 || diagonal21) {
                winner  = 2;
                gameOn = false;
            }
        }
        if (gameOn) {
            //If nobody won, and board is filled:
            if (boardFilled) {
                winner = 0;
                gameOn = false;
            }
        }
    }

    /**
     * Change turn
     */
    public void nextPlayer(){
        this.turn = !this.turn;
    }

    /**
     * AI set a piece on easy level
     * @return Return the position that AI set her piece
     */
    public int[] setPieceEasy(){
        return setRandomPiece();
    }

    /**
     * AI set a piece on medium level
     * @return Return the position that AI set her piece
     */
    public int[] setPieceMedium(){
        int[] positions = null;
        if((positions = tryTo3InARow()) == null) //If AI can not make 3 in a row, AI put a piece randomly
            positions = setRandomPiece();
        return positions;
    }

    /**
     * AI set a piece on hard level
     * @return Return the position that AI set her piece
     */
    public int[] setPieceHard(){
        int[] positions = null;
        if((positions = tryTo3InARow()) == null) //If AI can not make 3 in a row, AI try to stop you 3 in a row
            if((positions = cut3InARow()) == null) //If AI can not too, AI put a piece randomly
                positions = setRandomPiece();
        return positions;
    }

    /**
     * Set a piece randomly
     * @return Return the position that AI set her piece
     */
    private int[] setRandomPiece() {
        int min = 0,
                max= 3;
        int x=-1, y=-1; //Piece position
        Random random = new Random();
        do {
            x = random.nextInt(max - min) + min;
            y = random.nextInt(max - min) + min;
        }while (board[x][y] != EMPTY);
        return new int[]{x,y};
    }

    /**
     * Set a piece where AI can make 3 in a row
     * @return Return the position that AI set her piece
     */
    private int[] tryTo3InARow() {
        int[] positions = null;
        if (field12) {
            positions = new int[] {0,0};
        } else if (field22) {
            positions = new int[] {0,1};
        } else if (field32) {
            positions = new int[] {0,2};
        } else if (field42) {
            positions = new int[] {1,0};
        } else if (field52) {
            positions = new int[] {1,1};
        } else if (field62) {
            positions = new int[] {1,2};
        } else if (field72) {
            positions = new int[] {2,0};
        } else if (field82) {
            positions = new int[] {2,1};
        } else if (field92) {
            positions = new int[] {2,2};
        }
        return positions;
    }

    /**
     * Set a piece where AI can stop you 3 in a row
     * @return Return the position that AI set her piece
     */
    private int[] cut3InARow(){
        int[] positions = null;
        if (field11) {
            positions = new int[] {0,0};
        } else if (field21) {
            positions = new int[] {0,1};
        } else if (field31) {
            positions = new int[] {0,2};
        } else if (field41) {
            positions = new int[] {1,0};
        } else if (field51) {
            positions = new int[] {1,1};
        } else if (field61) {
            positions = new int[] {1,2};
        } else if (field71) {
            positions = new int[] {2,0};
        } else if (field81) {
            positions = new int[] {2,1};
        } else if (field91) {
            positions = new int[] {2,2};
        }
        return positions;
    }

    //Gets

    /**
     * Get who is the winner
     * @return Return who is the winner
     */
    public int getWinner(){
        return this.winner;
    }

    /**
     * Get game mode
     * @return Return game mode
     */
    public int getGameMode(){
        return this.gameMode;
    }

    /**
     * Get game difficulty
     * @return Return game difficulty
     */
    public int getDifficulty(){
        return this.difficulty;
    }

    /**
     * Get whose turn
     * @return Return whose turn
     */
    public boolean getTurn(){
        return this.turn;
    }

    /**
     * Get if game is on
     * @return Return if  game is on
     */
    public boolean isGameOn(){
        return this.gameOn;
    }
}