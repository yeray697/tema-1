package com.example.tema1.ejercicios;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * It is the first activity launched, and it contains 5 buttons to open the other exercises
 */
public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private Button btEjercicio1, btEjercicio2, btEjercicio3, btEjercicio4, btEjercicio5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Setting controls
        btEjercicio1 = (Button) findViewById(R.id.btEjercicio1);
        btEjercicio2 = (Button) findViewById(R.id.btEjercicio2);
        btEjercicio3 = (Button) findViewById(R.id.btEjercicio3);
        btEjercicio4 = (Button) findViewById(R.id.btEjercicio4);
        btEjercicio5 = (Button) findViewById(R.id.btEjercicio5);
        //Click events
        btEjercicio1.setOnClickListener(this);
        btEjercicio2.setOnClickListener(this);
        btEjercicio3.setOnClickListener(this);
        btEjercicio4.setOnClickListener(this);
        btEjercicio5.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;
        switch(v.getId()){
            case R.id.btEjercicio1:
                intent = new Intent(MainActivity.this,Exercise1_Activity.class);
                break;
            case R.id.btEjercicio2:
                intent = new Intent(MainActivity.this,Exercise2_Activity.class);
                break;
            case R.id.btEjercicio3:
                intent = new Intent(MainActivity.this,Exercise3_Activity.class);
                break;
            case R.id.btEjercicio4:
                intent = new Intent(MainActivity.this,Exercise4_Activity.class);
                break;
            case R.id.btEjercicio5:
                intent = new Intent(MainActivity.this,Exercise5_Activity.class);
                break;
            default:
                break;
        }
        if (intent != null)
            startActivity(intent);
    }
}
