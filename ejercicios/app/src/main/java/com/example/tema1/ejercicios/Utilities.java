package com.example.tema1.ejercicios;

/**
 * Static class with method used to convert
 */

public class Utilities {
    protected static final double ONECMISXINCH = 0.393701; //1 cm = 0.393701 inches
    //Updated dates at 23/09/2016
    protected static final double ONEDOLLARISXEUROS = 0.8931; //1 $ = 0.8931 €
    protected static final double ONEEUROISXDOLLAR = 1.1159; //1€ = 1.1159$


    /**
     * Method that convert centimeters to inches
     * @param centimeters Centimeters that will be converted
     * @return Return how many inches are X centimeters
     */
    public static double cmToInch(double centimeters)
    {
        // Rule of three:
        //        1 cm -> 0.393701
        // centimeters -> X

        double result = centimeters * ONECMISXINCH;
        return result;
    }

    /**
     * Method that convert dollars to euros
     * @param dollars Dollars that will be converted
     * @return Return how many euros are X dollars
     */
    public static double dollarToEuro(double dollars) {

        // Rule of three:
        // 1 dollars -> 0.8931
        //   dollars -> X

        double result = dollars * ONEDOLLARISXEUROS;
        return result;
    }

    /**
     * Method that convert euros to dollars
     * @param euros Euros that will be converted
     * @return Return how many dollars are X euros
     */
    public static double euroToDollar(double euros) {

        // Rule of three:
        // 1 euro -> 1.1159
        //  euros -> X

        double result = euros * ONEEUROISXDOLLAR;
        return result;
    }

    /**
     * Method that convert dollars to euros
     * @param dollars Dollars that will be converted
     * @param currency Currency data used to convert
     * @return Return how many euros are X dollars
     */
    public static double dollarToEuro(double dollars, double currency) {

        // Rule of three:
        // 1 dollars -> currency
        //   dollars -> X

        double result = dollars * currency;
        return result;
    }

    /**
     * Method that convert euros to dollars
     * @param euros Euros that will be converted
     * @param currency Currency data used to convert
     * @return Return how many dollars are X euros
     */
    public static double euroToDollar(double euros, double currency) {

        // Rule of three:
        // 1 euro -> currency
        //  euros -> X

        double result = euros * currency;
        return result;
    }
}
