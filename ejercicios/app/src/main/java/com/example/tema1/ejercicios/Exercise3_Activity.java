package com.example.tema1.ejercicios;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Exercise3_Activity extends AppCompatActivity implements View.OnClickListener{

    //
    // 3. Crear una aplicación que pida una dirección web y muestre un botón.
    // Cuando se pulse el botón, aparecerá una nueva ventana (una nueva actividad)
    // en la que se mostrará la web pedida.
    //

    protected static final String WEBINTENT = "web"; //Variable name put as Extra to the intent
    private static final String NOWEBERROR = "No has introducido ninguna web"; //Toast error message
    protected static final String WRONGERROR = "No has introducido una web válida"; //Toast error message

    private Button btSearch;
    private EditText etWeb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise3);
        //Changing the title
        setTitle(R.string.title3);
        //Setting controls
        btSearch = (Button) findViewById(R.id.btSearch3);
        etWeb = (EditText) findViewById(R.id.etWeb3);
        //Click events
        btSearch.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btSearch3:
                Search();
                break;
            default:
                break;
        }
    }

    /**
     * Method that open another activity with a WebView with the url set at etWeb EditText
     */
    private void Search() {
        //Getting url
        String web = etWeb.getText().toString();
        if (web.length() > 0) { //If it is a valid url...
            //Start a new activity
            Intent intent = new Intent(Exercise3_Activity.this, Exercise3_webview_Activity.class);
            intent.putExtra(WEBINTENT,web);
            startActivity(intent);
        }
        else
            Toast.makeText(this, NOWEBERROR, Toast.LENGTH_SHORT).show();
    }
}
