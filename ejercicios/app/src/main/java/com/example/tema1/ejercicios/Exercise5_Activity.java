package com.example.tema1.ejercicios;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

public class Exercise5_Activity extends AppCompatActivity implements View.OnClickListener {

    //
    // 5. Crear una aplicación propia similar a las de los ejercicios anteriores.
    // Además se enviará documentación (en formato html) explicando qué hace y cómo funciona.
    //
    // Chosen exercise: Tic Tac Toe
    //

    private RelativeLayout players_layout, difficulty_layout;
    private Button bt1player, bt2player;
    private Button btEasy, btMedium, btHard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise5);
        //Changing the title
        setTitle(R.string.title5);
        //Setting controls
        players_layout = (RelativeLayout) findViewById(R.id.players_layout);
        difficulty_layout = (RelativeLayout) findViewById(R.id.difficulty_layout);
        bt1player = (Button) findViewById(R.id.bt1player5);
        bt2player = (Button) findViewById(R.id.bt2player5);
        btEasy = (Button) findViewById(R.id.btEasy5);
        btMedium = (Button) findViewById(R.id.btMedium5);
        btHard = (Button) findViewById(R.id.btHard5);
        //Click event
        bt1player.setOnClickListener(this);
        bt2player.setOnClickListener(this);
        btEasy.setOnClickListener(this);
        btMedium.setOnClickListener(this);
        btHard.setOnClickListener(this);
        //Layout is hidden until user clicks One Player mode
        difficulty_layout.setVisibility(RelativeLayout.GONE);

    }

    @Override
    public void onBackPressed() {
        // If difficulty layout is INVISIBLE, it is because user had not selected the gamemode
        //  so, when user presses back, activity_exercise3 should close.
        // If difficulty layout is VISIBLE, it is because user had selected the 2 players mode
        //  so, when user presses back, difficulty_layour should close and show the gamemode selection.
        if(difficulty_layout.getVisibility() != RelativeLayout.GONE){
            players_layout.setVisibility(RelativeLayout.VISIBLE);
            difficulty_layout.setVisibility(RelativeLayout.GONE);
        } else
            super.onBackPressed();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.bt1player5:
                onePlayer();
                break;
            case R.id.bt2player5:
                twoPlayers();
                break;
            case R.id.btEasy5:
                easyGame();
                break;
            case R.id.btMedium5:
                mediumGame();
                break;
            case R.id.btHard5:
                hardGame();
                break;
            default:
                break;
        }
    }

    /**
     * Changing layouts visibility
     */
    private void onePlayer() {
        difficulty_layout.setVisibility(RelativeLayout.VISIBLE);
        players_layout.setVisibility(RelativeLayout.GONE);
    }

    /**
     * Start a 2 players' game
     */
    private void twoPlayers() {
        Intent intent = new Intent(Exercise5_Activity.this, TicTacToe_Activity.class);
        intent.putExtra(TicTacToe_Activity.MODEINTENT,TicTacToe_Activity.MODE2INTENT);
        startActivity(intent);
    }

    /**
     * Start an easy 1 player's game
     */
    private void easyGame() {
        Intent intent = new Intent(Exercise5_Activity.this, TicTacToe_Activity.class);
        intent.putExtra(TicTacToe_Activity.MODEINTENT,TicTacToe_Activity.MODE1INTENT);
        intent.putExtra(TicTacToe_Activity.LEVELINTENT,TicTacToe_Activity.LEVEL1INTENT);
        startActivity(intent);
    }

    /**
     * Start a medium 1 player's game
     */
    private void mediumGame() {
        Intent intent = new Intent(Exercise5_Activity.this, TicTacToe_Activity.class);
        intent.putExtra(TicTacToe_Activity.MODEINTENT, TicTacToe_Activity.MODE1INTENT);
        intent.putExtra(TicTacToe_Activity.LEVELINTENT,TicTacToe_Activity.LEVEL2INTENT);
        startActivity(intent);
    }

    /**
     * Start a hard 1 player's game
     */
    private void hardGame() {
        Intent intent = new Intent(Exercise5_Activity.this, TicTacToe_Activity.class);
        intent.putExtra(TicTacToe_Activity.MODEINTENT,TicTacToe_Activity.MODE1INTENT);
        intent.putExtra(TicTacToe_Activity.LEVELINTENT,TicTacToe_Activity.LEVEL3INTENT);
        startActivity(intent);
    }
}
