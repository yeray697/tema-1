package com.example.tema1.ejercicios;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class Exercise3_webview_Activity extends AppCompatActivity {

    private WebView wvEx3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise3_webview);
        //Changing the title
        setTitle(R.string.title3);
        //First one, we get the url from previous activity
        String url = getIntent().getExtras().getString(Exercise3_Activity.WEBINTENT);
        //Adding if necessary http://www.
        if(!url.startsWith("www.") && !url.startsWith("http://www.") && !url.startsWith("https://www.")){
            url = "www."+url;
        }
        if(!url.startsWith("http://") && !url.startsWith("https://")){
            url = "http://"+url;
        }
        wvEx3 = (WebView) findViewById(R.id.wvEx3);
        //Setting WebViewClient, because if it is not set, it opens Chrome
        wvEx3.setWebViewClient(new WebViewClient() {
            //Sobreescribimos este método para que en caso de error, muestre un Toast y devuelva al Activity anterior
            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
                Toast.makeText(getApplicationContext(), Exercise3_Activity.WRONGERROR, Toast.LENGTH_SHORT).show();
                finish();
            }
        });
        //Loagind url
        wvEx3.loadUrl(url);
    }
}
