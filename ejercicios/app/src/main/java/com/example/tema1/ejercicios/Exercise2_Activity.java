package com.example.tema1.ejercicios;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Locale;

public class Exercise2_Activity extends AppCompatActivity implements View.OnClickListener {

    //
    // 2. Crear una aplicación que pida un valor en centímetros y lo convierta a pulgadas
    //

    private static final String NONUMBERERROR = "Introduce un número";
    private Button btConvert;
    private EditText etCm, etInch;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise2);
        //Cambiando título
        setTitle(R.string.title2);
        //Guardando controles
        btConvert = (Button) findViewById(R.id.btConvert2);
        etCm = (EditText) findViewById(R.id.etCm2);
        etInch = (EditText) findViewById(R.id.etInch2);
        //Eventos click
        btConvert.setOnClickListener(this);
        //Deshabilitamos el EditText etInch
        etInch.setEnabled(false);
    }

    @Override
    public void onClick(View view) {
        try {
            //Obtenemos los centímetros del EditText
            double cm = Double.parseDouble(etCm.getText().toString());
            //Los convertimos a pulgadas
            double inch = Utilities.cmToInch(cm);
            //Lo mostramos en el otro EditText redondeado a 2 decimales
            etInch.setText(String.format(Locale.US, "%.2f", inch) + " pulgadas");
        }
        catch (Exception ex) {
            Toast.makeText(this, NONUMBERERROR, Toast.LENGTH_SHORT).show();
        }
    }
}
